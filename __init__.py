bl_info = {
    "name": "Safe Save",
    "description": "Verify that we don't override a published version",
    "author": "Les Fees Speciales (LFS)",
    "version": (1, 0),
    "blender": (2, 91)
}

import bpy
from bpy.app.handlers import persistent
from .prevent_save import SafeSaveOperator

@persistent
def override_save_operator(dummy):
    try:
        bpy.context.window_manager.keyconfigs['blender'].keymaps['Window'].keymap_items['wm.save_mainfile'].idname = "wm.safe_save_mainfile"
    except:
        pass

def register():
    bpy.utils.register_class(SafeSaveOperator)
    bpy.app.handlers.load_post.append(override_save_operator)
    

def unregister():
    bpy.app.handlers.load_post.remove(override_save_operator)
    bpy.utils.unregister_class(SafeSaveOperator)
    try:
        bpy.context.window_manager.keyconfigs['blender'].keymaps['Window'].keymap_items['wm.safe_save_mainfile'].idname = "wm.save_mainfile"
    except:
        pass