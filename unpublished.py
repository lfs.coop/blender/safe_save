import bpy
from bpy.app.handlers import persistent
import os
import time

nb_hours = 48
nb_seconds = nb_hours * 60 * 60

def age_of_file(file_name):
    last_modif = os.path.getctime(file_name)
    now = time.monotonic()
    return abs(now - last_modif)


def too_old():
    return age_of_file(bpy.path.abspath("//")) >= nb_seconds


