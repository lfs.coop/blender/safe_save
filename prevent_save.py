import bpy
from .unpublished import too_old
# Returns False if the file was published (<=> returns True if the file can be saved)

def check_published():
    path = bpy.path.abspath('//')
    path = path.replace('\\', '/').lower()

    if len(path) == 0:
        return True
    else:
        s_path = path.split('/')
        s_path.pop() # Dump file name
        parent = s_path.pop()

        if (parent[0].lower() == 'v') and (parent[1:-1].isdigit()):
            return False
        else:
            return True

class SafeSaveOperator(bpy.types.Operator):

    """ Verifies that we don't override a publishe version """

    bl_idname = "wm.safe_save_mainfile"
    bl_label = "Save"

    proceed: bpy.props.BoolProperty(name="Proceed anyway ?", default=False)

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.label(text="This file has already been published")
        row = layout.row()
        row.prop(self, 'proceed')


    def invoke(self, context, event):
        
        if not check_published():
            return context.window_manager.invoke_props_dialog(self)
        else:
            return self.execute(context)

    def execute(self, context):
        if check_published() or self.proceed:
            if too_old():
                self.report({'ERROR'}, "This working copy is too old. Publish it")
                return {'CANCELLED'}
            else:
                bpy.ops.wm.save_mainfile()
                self.report({'INFO'}, "Saved to: {}".format(bpy.path.abspath('//')))
                return {'FINISHED'}
        else:
            self.report({'ERROR'}, "Saving on published file cancelled")
            return {'CANCELLED'}
            


